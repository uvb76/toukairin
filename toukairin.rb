require 'discordrb'
require 'hashie'

settings = Hashie::Mash.load("settings.yaml")

bot = Discordrb::Bot.new token: settings.token, client_id: settings.client_id

bot.message(with_text: 'Ping!') do |event|
  event.respond 'Pong!'
end

bot.message(with_text: '東海林') do |event|
  event.respond 'えぇ～'
end

bot.message(with_text: 'roll!') do |event|
  event.respond (1..6).to_a.sample.to_s
end

bot.run
